#include <iostream>
#include <memory>

// allow commands
const short ADD = 1;
const short DELETE = 2;
const short SEARCH_MEDIAN = 3;
const short END = 4;

struct TNode {
  long value = 0;
  long height = 0;
  std::shared_ptr<TNode> left = nullptr;
  std::shared_ptr<TNode> right = nullptr;
};

// maximum of two longs
long max(long a, long b)
{
  return (a > b)? a : b;
}

long height(std::shared_ptr<TNode>& root)
{
  return (root == nullptr ? -1 : root->height);
}

void show(const std::shared_ptr<TNode>& root)
{
  if (root == nullptr)
    return;

  show(root->left);
  std::cout << "V: " << root->value << " " << "H: " << root->height << "\n";
  show(root->right);
}

std::shared_ptr<TNode> find(const std::shared_ptr<TNode>& root, long value)
{
  if (root == nullptr)
    return nullptr;

  if (root->value == value)
    return root;

  if (root->value > value)
    return find(root->left, value);

  if (root->value < value)
    return find(root->right, value);

  return nullptr;
}

std::shared_ptr<TNode> rightRotate(std::shared_ptr<TNode>& root)
{
  std::shared_ptr<TNode> u = root->left;
  root->left =u->right;
  u->right = root;

  root->height = max(height(root->left), height(root->right)) + 1;
  u->height = max(height(u->left), root->height) + 1;

  return u;
}

std::shared_ptr<TNode> leftRotate(std::shared_ptr<TNode>& root)
{
  std::shared_ptr<TNode> u = root->right;
  root->right = u->left;
  u->left = root;

  root->height = max (height(root->left), height(root->right)) + 1;
  u->height = max(height(root->right), root->height) + 1;

  return u;
}

std::shared_ptr<TNode> doubleLeftRotate(std::shared_ptr<TNode>& root)
{
  root->right = rightRotate(root->right);
  return leftRotate(root);
}

std::shared_ptr<TNode> doubleRightRotate(std::shared_ptr<TNode>& root)
{
  root->left = leftRotate(root->left);
  return rightRotate(root);
}

std::shared_ptr<TNode> insert(std::shared_ptr<TNode> root, long value)
{
  if (root == nullptr) {
    root.reset(new TNode);
    root->value = value;
  }

  else if (value < root->value) {
    root->left = insert(root->left, value);
    if (height(root->left) - height(root->right) == 2) {
      if (value < root->left->value)
        root = rightRotate(root);
      else
        root = doubleRightRotate(root);
    }
  }

  else if (value > root->value) {
    root->right = insert(root->right, value);
    if (height(root->right) - height(root->left) == 2) {
      if (value > root->right->value)
        root = leftRotate(root);
      else
        root = doubleLeftRotate(root);
    }
  }

  root->height = max(height(root->left), height(root->right)) + 1;;

  return root;
}


std::shared_ptr<TNode> min(std::shared_ptr<TNode>& root)
{
  if (root->left == nullptr)
    return  root;

  min(root->left);
}

int i = 1;
std::shared_ptr<TNode> delete_node(std::shared_ptr<TNode>& root, ulong value)
{
  std::cout << i++ << "\n";
  if (root == nullptr)
    return nullptr;

  if (value < root->value)
    root->left = delete_node(root->left, value);

  else if (value > root->value)
    root->right = delete_node(root->right, value);

  else if (value == root->value) {
    if (root->right == nullptr)
      std::cout << "Right nullptr\n";

    if (root->left == nullptr)
      std::cout << "Left nullptr\n";

    if (root->right == nullptr && root->left == nullptr)
      return nullptr;

    else if (root->left == nullptr)
      return root->right;

    else if (root->right == nullptr)
      return root->left;

    std::shared_ptr<TNode> successor = min(root->right);
    root->value = successor->value;

    root->right = delete_node(root->right, successor->value);
  }

  return root;
}

int main()
{
  std::shared_ptr<TNode> root = nullptr;
//  std::shared_ptr<TNode> min_node = nullptr;

  short command = 0;
  while (command != END) {
    std::cin >> command;

    switch (command) {
      case ADD: {
        long value;
        std::cin >> value;

        root = insert(root, value);
        std::cout << "ROOT: " << root->value << "\n";
        show(root);
      } break;
      case DELETE: {
        long value;
        std::cin >> value;
        i = 0;
        auto node = find(root, value);
        if (node != nullptr) {
          root = delete_node(root, value);
          std::cout << "DELETE:\n";
          show(root);
        }
        else
          std::cout << "NOT FIND\n";
      } break;
      case SEARCH_MEDIAN: {

      } break;
      default:
        return 1;
    }
  }

  return 0;
}