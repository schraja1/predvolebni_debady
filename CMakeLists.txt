cmake_minimum_required(VERSION 3.7)
project(predvolebni_debady)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        CMakeLists.txt
        main.cpp
        README.md)

add_executable(predvolebni_debady ${SOURCE_FILES})